using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Core;
using Microsoft.Extensions.Logging;

namespace webapi.Controllers
{
    public class HomeController : ControllerBase
    {
        [Authorize]
        [Route("hello-user")]
        public IActionResult Get() 
        {
            return Content("Hello User!");
        }

        public IActionResult NotAuthorized() 
        {
            return Content("Not Authorized");
        }
    }
}